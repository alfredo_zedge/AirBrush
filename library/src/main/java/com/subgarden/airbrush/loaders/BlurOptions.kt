package com.subgarden.airbrush.loaders

/**
 * @author Fredrik Larsen (f@subgarden.com)
 */
data class BlurOptions(val radius: Float, val sampleSize: Int = 6)